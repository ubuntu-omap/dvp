#!/bin/bash -xe

unset CFLAGS
unset LDFLAGS
unset CPPFLAGS
unset CXXFLAGS
unset FFLAGS

export KVERSION=`uname -r`
export KERNEL_ROOT=/usr/src/linux-headers-${KVERSION}/include/linux
export DVP_ROOT=`pwd`
export SOSAL_ROOT=`pwd`
export CROSS_COMPILE=
export TARGET_CPU=ARM
export TARGET_PLATFORM=PANDA
unset MYDROID

export TARGET_BUILD=debug # for gdb symbols
export BUILD_DEBUG=1 # This is to see all the comand line options used for compiling each file during the build
export DVP_NO_OPTIMIZE=1 # This is to turn off the optimizations (Default is release mode, this puts in debug mode)

cd $DVP_ROOT/scripts

#  build

./dvp.sh zone perf mm
